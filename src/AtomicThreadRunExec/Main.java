package AtomicThreadRunExec;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {

    public static int RandomNumber(int min, int max){
        int randoInt = (int) (Math.random()*(max-min+1) + min);
        return randoInt;
    }

    public static void main(String[] args)  {
        // This statement creates the atomic variable and sets it's value.
        AtomicInteger theMath = new AtomicInteger(0);

        // This statement creates the executor service and sets the number of threads it will service at a time.
        ExecutorService theExecutioner = Executors.newFixedThreadPool(2);

        // This creates the instances of the RandomMath Runnable classes used in the executor.
        RandomMath benMath = new RandomMath("Benny", theMath);
        RandomMath hannahMath = new RandomMath("Hannah", theMath);
        RandomMath chuckMath = new RandomMath("Chuck", theMath);

        // This creates the lone runnable and the lone thread.
        //Runnable
        Thread chuckRun = new Thread(chuckMath);
        //Thread
        Thread theJerk = new ResetJerk(theMath, "The Jerk");


        // This starts the executor service for two of the instances.
        theExecutioner.execute(benMath);
        theExecutioner.execute(hannahMath);

        // This starts both the lone runnable and lone thread.
        chuckRun.start();
        theJerk.start();

        // This makes sure that the lone runnable and the thread will wait for the other threads to finish before it finishes.
        try {
            chuckRun.join();
            theJerk.join();
        } catch (InterruptedException e) {
            System.out.println("Unable to wait for other threads to stop before terminating. Process was interrupted.");
        }


        // This statement shuts down the executor service so the program does not hang and thread execution has finished.
        theExecutioner.shutdown();
    }
}


