package AtomicThreadRunExec;

import java.util.concurrent.atomic.AtomicInteger;

// This is the class that extends Thread.
public class ResetJerk extends Thread{
    AtomicInteger totalMath;
    String name;

    public ResetJerk(AtomicInteger totalMath, String name) {
        this.totalMath = totalMath;
        this.name = name;
    }

    public void run(){
        Thread.currentThread().setName(name);
        int randoReset = Main.RandomNumber(2000, 4000);
        int randResAmount = Main.RandomNumber(1, 4);
        for (int i = 0; i < randResAmount; i++) {

            try {
               Thread.sleep(randoReset);
            } catch (InterruptedException e) {
                System.out.println("Unable to be a jerk. Process interrupted.");
            }
            totalMath.set(0);
            System.out.println(Thread.currentThread().getName() + " will set the value back to zero! What a jerk...");
            System.out.println("New total is: " + totalMath.get());
        }
    }
}
