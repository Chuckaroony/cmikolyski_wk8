package AtomicThreadRunExec;

import java.util.concurrent.atomic.AtomicInteger;

// This is the class that implements Runnable.
public class RandomMath implements Runnable {

    AtomicInteger totalMath;
    String name;

    RandomMath (String name, AtomicInteger theMath) {
        this.name = name;
        this.totalMath = theMath;
    }

    public void run() {
        Thread.currentThread().setName(name);
        for (int i = 0; i < 5; i++) {
            int rando = Main.RandomNumber(1, 2);
            int randoForMath = Main.RandomNumber(1, 100);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                System.out.println("The process was interrupted.");
            }
            if (rando == 1) {
                System.out.println(Thread.currentThread().getName()+" will add: " +randoForMath);
                totalMath.getAndAdd(randoForMath);
                System.out.println("New total is: " +totalMath.get());
            }
            else{
                System.out.println(Thread.currentThread().getName()+" will subtract: " +randoForMath);
                totalMath.getAndAdd(-randoForMath);
                System.out.println("New total is: " +totalMath.get());
            }
        }
    }
}
