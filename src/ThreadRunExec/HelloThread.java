package ThreadRunExec;

public class HelloThread extends Thread{
    public void run() {
        try {
            System.out.println(Thread.currentThread().getName());
            System.out.println("Hello from a thread!");
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String args[]) {
        (new HelloThread()).start();
    }
}
