package ThreadRunExec;

public class HelloRunnable implements Runnable{
    public void run() {
        try {
            System.out.println(Thread.currentThread().getName());
            System.out.println("Hello from a runnable thread!");
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        (new Thread(new HelloRunnable())).start();
    }


}


